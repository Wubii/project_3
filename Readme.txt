Options +FollowSymLinks  // active les liens symboliques
RewriteEngine On         // active le module de réécriture des urls
RewriteRule ^(.*)$ index.php [NC,L]  // prend tout et balance dans le fichier index.php. Point de départ


router = fait le lien entre l’url et l’action, la fonction demandée


Twig téléchargé avec git

private = la variable nest pas visible de l’extérieur de la classe. Private static = la variable existe partout mais n’est pas visible de l’extérieur de la classe. 

protected = la variable nest pas visible depuis l’extérieur de la classe sauf pour les descendants.

public static = la variable est visible de l’extérieur de la classe. Pas besoin d’instancier la classe pour travailler avec.

singleton = une class qui ne sera instanciee qu'une seule fois dans toute l'application (ex: connexion bdd)



Add from bitbucket
